import React from 'react';
import Banner from '../Banner/Banner';
import List from '../List/List';

const HomePage = () => {
  return (
    <>
      <Banner></Banner>
      <List></List>
    </>
  );
};

export default HomePage;
