import { Button, Image } from '@mantine/core';
import React, { useState } from 'react';
import {
  SHeader,
  SHeaderContainer,
  SHeaderList,
  SHeaderLink,
  Triangle,
} from './styles';
import { HeaderData } from './HeaderData';
import { useHistory } from 'react-router-dom';

const Header = () => {
  const history = useHistory();
  const [headerLinks, setHeaderLinks] = useState({
    isLoading: false,
    items: [...HeaderData],
    selected: null,
  });
  return (
    <SHeader>
      <SHeaderContainer>
        <a href="/">
          <Image
            radius="md"
            src="https://gameplus.com.tr/static/media/logo.6d064b08.png"
            alt="Logo"
            width={300}
            height={50}
          />
        </a>
        <SHeaderList>
          {headerLinks?.items.map((item, index) => {
            return (
              <li key={index}>
                <SHeaderLink
                  href="/"
                  onClick={(e) => {
                    e.preventDefault();
                    setHeaderLinks((prevState) => ({
                      ...prevState,
                      selected: item,
                    }));
                    history.push(item.path);
                  }}
                >
                  {item.label}
                  <Triangle active={history.location.pathname === item.path} />
                </SHeaderLink>
              </li>
            );
          })}
        </SHeaderList>
        <Button
          styles={() => ({
            root: {
              color: '#fff',
              backgroundColor: '#76b900',
              border: 0,
              height: 42,
              paddingLeft: 20,
              paddingRight: 20,
              '&:hover': {
                backgroundColor: '#76b900',
              },
            },
          })}
        >
          LET&apos;S PLAY
        </Button>
      </SHeaderContainer>
    </SHeader>
  );
};

export default Header;
