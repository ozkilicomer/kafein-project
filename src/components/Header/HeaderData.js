export const HeaderData = [
  {
    id: 1,
    label: 'Games',
    path: '/',
    active: true,
  },
  {
    id: 2,
    label: 'Membership',
    path: '/membership',
    active: false,
  },
  {
    id: 3,
    label: 'Download',
    path: '/download',
    active: false,
  },
  {
    id: 4,
    label: 'Blog',
    path: '/blog',
    active: false,
  },
  {
    id: 5,
    label: 'Support',
    path: '/support',
    active: false,
  },
];
