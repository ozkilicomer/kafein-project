import styled from 'styled-components';

export const SHeader = styled.div`
  background: #fff;
  height: 70px;
`;

export const SHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 1280px;
  margin: 0 auto;
  padding: 10px;
`;

export const SHeaderList = styled.ul`
  display: flex;
  align-items: center;
  padding: 0;
  list-style: none;
`;

export const SHeaderLink = styled.a`
  position: relative;
  text-decoration: none;
  color: #000;
  font-size: 16px;
  font-weight: 600;
  letter-spacing: 0.57px;
  cursor: pointer;
  margin-left: 25px;
`;

export const Triangle = styled.div`
  display: ${(props) => (props.active === true ? 'inline-block' : 'none')};
  position: absolute;
  width: 0;
  height: 0;
  left: calc(50% - 10px);
  bottom: -26px;
  border-style: solid;
  border-width: 0 10px 17.3px 10px;
  border-color: transparent transparent #76b900 transparent;
`;
