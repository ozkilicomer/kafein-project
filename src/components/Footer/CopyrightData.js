export const CopyrightData = [
  {
    id: 1,
    label: 'Terms Of Use',
    link: '/terms-of-use',
  },
  {
    id: 2,
    label: 'Privacy Policy',
    link: '/privacy-policy',
  },
  {
    id: 3,
    label: 'Cookies',
    link: '/cookies',
  },
];
