export const FooterData = [
  {
    id: 1,
    label: 'Games',
    link: '/games',
  },
  {
    id: 2,
    label: 'Contact Us',
    link: '/contact-us',
  },
  {
    id: 3,
    label: 'FAQs',
    link: '/faqs',
  },
  {
    id: 4,
    label: 'Membership',
    link: '/membership',
  },
  {
    id: 5,
    label: 'Blog',
    link: '/blog',
  },
  {
    id: 6,
    label: 'Service Status',
    link: '/service-status',
  },
  {
    id: 7,
    label: 'Download',
    link: '/download',
  },
];
