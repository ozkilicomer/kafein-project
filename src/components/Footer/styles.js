import styled from 'styled-components';

export const SFooter = styled.div`
  color: #000;
  background: #fff;
  height: 147px;
`;

export const SFooterContent = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  padding: 24px 10px;
`;

export const SFooterLeft = styled.div`
  width: 50%;
`;

export const SFooterRight = styled.div`
  display: flex;
  width: 50%;
  justify-content: end;
`;

export const SFooterList = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  text-decoration: none;
  color: #000;
  cursor: pointer;
`;

export const SFooterLinkLi = styled.li`
  list-style: none;
  cursor: pointer;
  flex-basis: 25%;
  margin-right: 5px;
`;

export const SFooterLinkA = styled.a`
  text-decoration: none;
  color: #1e1e1e;
`;

export const SFooterRightSocialMedia = styled.div`
  display: flex;
  flex-direction: column;
`;

export const SFooterRightSocialMediaUl = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  text-decoration: none;
  color: #000;
`;

export const SFooterRightLang = styled.div`
  margin-left: 2rem;
  color: #1e1e1e;
`;

export const SFooterCopyright = styled.div`
  background-color: #1e1e1e;
  color: #fff;
  padding: 0 10px;
`;

export const SFooterCopyrightContainer = styled.div`
  display: flex;
  max-width: 1280px;
  margin: 0 auto;
  justify-content: space-between;
  padding: 10px;
  align-items: center;
`;

export const SFooterCopyrightRightSide = styled.div`
  display: flex;
  width: 50%;
`;

export const SFooterCopyrightLeftSide = styled.div`
  display: flex;
  width: 50%;
  justify-content: end;
  font-size: 12px;
  font-weight: 400;
`;

export const CopyrightLinkA = styled.a`
  margin-right: 20px;
  font-size: 12px;
  font-weight: 600;
  line-height: 1.83;
  color: #fff;
  text-decoration: none;
  cursor: pointer;
`;
