import React, { useState } from 'react';
import {
  SFooter,
  SFooterContent,
  SFooterLeft,
  SFooterRight,
  SFooterCopyright,
  SFooterList,
  SFooterLinkLi,
  SFooterLinkA,
  SFooterCopyrightContainer,
  SFooterRightSocialMedia,
  SFooterRightLang,
  SFooterRightSocialMediaUl,
  SFooterCopyrightRightSide,
  SFooterCopyrightLeftSide,
  CopyrightLinkA,
} from './styles';
import { FooterData } from './FooterData';
import { SocialMediaData } from './SocialMediaData';
import { LangData } from './LangData';
import { CopyrightData } from './CopyrightData';
import { Select } from '@mantine/core';

const Footer = () => {
  const [footerLinks] = useState({
    isLoading: false,
    items: [...FooterData],
  });

  const [socialMediaLinks] = useState({
    isLoading: false,
    items: [...SocialMediaData],
  });

  const [langs, setLangs] = useState({
    isLoading: false,
    items: [...LangData],
    selected: 'EN',
  });

  const [copyRight] = useState({
    isLoading: false,
    items: [...CopyrightData],
  });

  return (
    <SFooter>
      <SFooterContent>
        <SFooterLeft>
          <SFooterList>
            {footerLinks?.items.map((item, index) => {
              return (
                <SFooterLinkLi key={index}>
                  <SFooterLinkA
                    href="/"
                    onClick={(e) => {
                      e.preventDefault();
                    }}
                  >
                    {item.label}
                  </SFooterLinkA>
                </SFooterLinkLi>
              );
            })}
          </SFooterList>
        </SFooterLeft>
        <SFooterRight>
          <SFooterRightSocialMedia>
            <span style={{ marginBottom: '10px' }}>Follow Us!</span>
            <SFooterRightSocialMediaUl>
              {socialMediaLinks?.items.map((item, index) => {
                return (
                  <SFooterLinkLi key={index}>
                    <SFooterLinkA href={item.link} target="_blank">
                      {item.icon}
                    </SFooterLinkA>
                  </SFooterLinkLi>
                );
              })}
            </SFooterRightSocialMediaUl>
          </SFooterRightSocialMedia>
          <SFooterRightLang>
            <span style={{ marginBottom: '10px' }}>Site Language</span>
            <Select
              value={langs?.selected}
              data={langs?.items}
              onChange={(value) => {
                setLangs((prevState) => ({
                  ...prevState,
                  selected: value,
                }));
              }}
            />
          </SFooterRightLang>
        </SFooterRight>
      </SFooterContent>
      <SFooterCopyright>
        <SFooterCopyrightContainer>
          <SFooterCopyrightRightSide>
            {copyRight?.items.map((item, index) => {
              return (
                <CopyrightLinkA key={index} href={item.link} target="_blank">
                  {item.label}
                </CopyrightLinkA>
              );
            })}
          </SFooterCopyrightRightSide>
          <SFooterCopyrightLeftSide>
            Tüm Hakları Saklıdır.
          </SFooterCopyrightLeftSide>
        </SFooterCopyrightContainer>
      </SFooterCopyright>
    </SFooter>
  );
};

export default Footer;
