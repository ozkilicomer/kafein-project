import React from 'react';
import {
  BrandFacebook,
  BrandInstagram,
  BrandYoutube,
  BrandTwitter,
} from 'tabler-icons-react';

export const SocialMediaData = [
  {
    id: 1,
    label: 'Facebook',
    link: 'www.facebook.com',
    icon: <BrandFacebook />,
  },
  {
    id: 2,
    label: 'İnstagram',
    link: 'www.instagram.com',
    icon: <BrandInstagram />,
  },
  {
    id: 3,
    label: 'Youtube',
    link: 'www.youtube.com',
    icon: <BrandYoutube />,
  },
  {
    id: 4,
    label: 'Twitter',
    link: 'www.twitter.com',
    icon: <BrandTwitter />,
  },
];
