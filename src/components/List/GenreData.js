export const GenreData = [
  {
    id: 1,
    label: 'Shooter',
    slug: 'SHOOTER',
  },
  {
    id: 2,
    label: 'Action',
    slug: 'ACTION',
  },
  {
    id: 3,
    label: 'RPG',
    slug: 'RPG',
  },
  {
    id: 4,
    label: 'Racing',
    slug: 'RACING',
  },
  {
    id: 5,
    label: 'MOBA',
    slug: 'MOBA',
  },
];
