import styled from 'styled-components';

export const SList = styled.div`
  background-color: #000000;
`;

export const SListContainer = styled.div`
  display: flex;
  justify-content: space-between;
  max-width: 1240px;
  width: 100%;
  margin: 0 auto;
  padding: 64px 20px;
`;

export const SListFilterArea = styled.div`
  width: 33%;
`;

export const SContentArea = styled.div`
  width: 66%;
`;

export const SListFilterBox = styled.div`
  background-color: #1e1e1e;
  padding: 40px 30px 60px;
  width: 100%;
  max-width: 386px;
`;

export const FilterBoxState = styled.div`
  margin-bottom: 50px;
`;

export const FilterBoxGenre = styled.div`
  margin-bottom: 50px;
`;

export const Hexagon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  width: 50px;
  height: 28.87px;
  background-color: #76b900;
  margin: 14.43px 0px 40px 0px;
  font-weight: 700;
  ::before,
  ::after {
    content: '';
    position: absolute;
    width: 0;
    border-left: 25px solid transparent;
    border-right: 25px solid transparent;
  }
  :before {
    bottom: 100%;
    border-bottom: 14.43px solid #76b900;
  }
  :after {
    top: 100%;
    width: 0;
    border-top: 14.43px solid #76b900;
  }
`;

export const GamesA = styled.a`
  flex-grow: 1;
  flex-basis: 50%;
  color: #afafaf;
  margin-bottom: 16px;
  cursor: pointer;
`;
