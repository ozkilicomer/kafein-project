// export const GamesData = [
//   {
//     id: 1,
//     label: 'A',
//     slug: 'A',
//     games: [
//       {
//         id: 1,
//         label: 'A Games 1',
//         genre: 'SHOOTER',
//         state: 'AVALIBLE',
//         slug: 'A',
//       },
//       {
//         id: 2,
//         label: 'A Games 2',
//         genre: 'SHOOTER',
//         state: 'AVALIBLE',
//         slug: 'A',
//       },
//       {
//         id: 3,
//         label: 'A Games 3',
//         genre: 'MOBA',
//         state: 'MAINTENANCE',
//         slug: 'A',
//       },
//       {
//         id: 4,
//         label: 'A Games 4',
//         genre: 'RACING',
//         state: 'PATCHING',
//         slug: 'A',
//       },
//       {
//         id: 5,
//         label: 'A Games 5',
//         genre: 'SHOOTER',
//         state: 'AVALIBLE',
//         slug: 'A',
//       },
//     ],
//   },
//   {
//     id: 2,
//     label: 'B',
//     slug: 'B',
//     games: [
//       {
//         id: 1,
//         label: 'B Games 1',
//         genre: 'ACTION',
//         state: 'MAINTENANCE',
//         slug: 'B',
//       },
//       {
//         id: 2,
//         label: 'B Games 2',
//         genre: 'SHOOTER',
//         state: 'AVALIBLE',
//         slug: 'B',
//       },
//       {
//         id: 3,
//         label: 'B Games 3',
//         genre: 'MOBA',
//         state: 'MAINTENANCE',
//         slug: 'B',
//       },
//       {
//         id: 4,
//         label: 'B Games 4',
//         genre: 'RACING',
//         state: 'AVALIBLE',
//         slug: 'B',
//       },
//       {
//         id: 5,
//         label: 'B Games 5',
//         genre: 'ACTION',
//         state: 'AVALIBLE',
//         slug: 'B',
//       },
//     ],
//   },
//   {
//     id: 3,
//     label: 'C',
//     slug: 'C',
//     games: [
//       {
//         id: 1,
//         label: 'C Games 1',
//         genre: 'AVALIBLE',
//         state: 'MAINTENANCE',
//         slug: 'C',
//       },
//       {
//         id: 2,
//         label: 'C Games 2',
//         genre: 'RPG',
//         state: 'AVALIBLE',
//         slug: 'C',
//       },
//       {
//         id: 3,
//         label: 'C Games 3',
//         genre: 'RPG',
//         state: 'AVALIBLE',
//         slug: 'C',
//       },
//       {
//         id: 4,
//         label: 'C Games 4',
//         genre: 'MOBA',
//         state: 'AVALIBLE',
//         slug: 'C',
//       },
//       {
//         id: 5,
//         label: 'C Games 5',
//         genre: 'ACTION',
//         state: 'AVALIBLE',
//         slug: 'C',
//       },
//     ],
//   },
// ];

export const GamesData = [
  {
    label: 'A Games 1',
    genre: 'SHOOTER',
    state: 'AVALIBLE',
    slug: 'A',
    letterLabel: 'A',
  },
  {
    label: 'A Games 2',
    genre: 'SHOOTER',
    state: 'AVALIBLE',
    slug: 'A',
    letterLabel: 'A',
  },
  {
    label: 'A Games 3',
    genre: 'MOBA',
    state: 'MAINTENANCE',
    slug: 'A',
    letterLabel: 'A',
  },
  {
    label: 'A Games 4',
    genre: 'RACING',
    state: 'PATCHING',
    slug: 'A',
    letterLabel: 'A',
  },
  {
    label: 'A Games 5',
    genre: 'SHOOTER',
    state: 'AVALIBLE',
    slug: 'A',
    letterLabel: 'A',
  },
  {
    label: 'B Games 1',
    genre: 'ACTION',
    state: 'MAINTENANCE',
    slug: 'B',
    letterLabel: 'B',
  },
  {
    label: 'B Games 2',
    genre: 'SHOOTER',
    state: 'AVALIBLE',
    slug: 'B',
    letterLabel: 'B',
  },
  {
    label: 'B Games 3',
    genre: 'MOBA',
    state: 'MAINTENANCE',
    slug: 'B',
    letterLabel: 'B',
  },
  {
    label: 'B Games 4',
    genre: 'RACING',
    state: 'AVALIBLE',
    slug: 'B',
    letterLabel: 'B',
  },
  {
    label: 'B Games 5',
    genre: 'ACTION',
    state: 'AVALIBLE',
    slug: 'B',
    letterLabel: 'B',
  },
  {
    label: 'C Games 1',
    genre: 'AVALIBLE',
    state: 'MAINTENANCE',
    slug: 'C',
    letterLabel: 'C',
  },
  {
    label: 'C Games 2',
    genre: 'RPG',
    state: 'AVALIBLE',
    slug: 'C',
    letterLabel: 'C',
  },
  {
    label: 'C Games 3',
    genre: 'RPG',
    state: 'AVALIBLE',
    slug: 'C',
    letterLabel: 'C',
  },
  {
    label: 'C Games 4',
    genre: 'MOBA',
    state: 'AVALIBLE',
    slug: 'C',
    letterLabel: 'C',
  },
  {
    label: 'C Games 5',
    genre: 'ACTION',
    state: 'AVALIBLE',
    slug: 'C',
    letterLabel: 'C',
  },
];
