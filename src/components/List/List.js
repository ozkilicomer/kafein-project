import React, { useState } from 'react';
import {
  SList,
  SListContainer,
  SListFilterArea,
  SContentArea,
  SListFilterBox,
  FilterBoxState,
  FilterBoxGenre,
  Hexagon,
  GamesA,
} from './styles';
import { StateData } from './StateData';
import { GenreData } from './GenreData';
import { GamesData } from './GamesData';
import { Checkbox } from '@mantine/core';
import { ChevronDown } from 'tabler-icons-react';
import { Select } from '@mantine/core';

const List = () => {
  const [stateContent] = useState({
    isLoading: false,
    items: [...StateData],
  });

  const [genreContent] = useState({
    isLoading: false,
    items: [...GenreData],
    selected: [],
  });

  const [gamesContent, setGamesContent] = useState({
    isLoading: false,
    items: [...GamesData],
  });

  const [sortData, setSortData] = useState({
    isLoading: false,
    items: ['Title A-Z', 'Title Z-A'],
    selected: 'Title A-Z',
  });

  const sortGames = (sort) => {
    let sortedList = [];
    if (sort === 'Title A-Z') {
      sortedList = gamesContent?.items.sort(function (a, b) {
        return a.slug === b.slug ? 0 : a.slug < b.slug ? -1 : 1;
      });
    } else {
      sortedList = gamesContent?.items.sort(function (a, b) {
        return b.slug === a.slug ? 0 : b.slug < a.slug ? -1 : 1;
      });
    }
    setGamesContent((prevState) => ({
      ...prevState,
      items: sortedList,
    }));
  };

  const prepareData = (dataParams) => {
    if (dataParams.length > 0) {
      let data = dataParams
        ?.map((item) => item)
        .flat()
        .reduce((a, b) => {
          return [
            {
              ...a[0],
              [b.slug]:
                [...((a.length && a[0] && a[0][b.slug]) || []), b] || [],
            },
          ];
        }, []);
      data = data[0];
      data = Object.keys(data).map((key) => {
        return {
          letterLabel: key,
          slug: key,
          games: data[key],
        };
      });
      return data;
    } else {
      return [];
    }
  };

  return (
    <SList>
      <SListContainer>
        <SListFilterArea>
          <h1 style={{ color: 'white', margin: 0, marginBottom: '1rem' }}>
            Browse Game
          </h1>
          <SListFilterBox>
            <FilterBoxState>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  cursor: 'pointer',
                  marginBottom: '20px',
                }}
              >
                <h2 style={{ color: '#afafaf', fontSize: '24px' }}>
                  Game Status
                </h2>
                <ChevronDown color="white"></ChevronDown>
              </div>
              {stateContent?.items.map((item, index) => {
                return (
                  <div key={index} style={{ marginBottom: '10px' }}>
                    <Checkbox
                      size="md"
                      styles={{
                        root: { color: '#76b900' },
                        inner: { color: '#76b900' },
                        label: { color: '#afafaf' },
                        icon: { color: 'white' },
                      }}
                      label={item.label}
                      checked={item.checked}
                      onChange={(event) => {
                        if (event.currentTarget.checked) {
                          const filteredArr = gamesContent.items.filter(
                            (ele) => ele.state === item.slug
                          );
                          const uniq = [
                            ...new Set(filteredArr.map(({ label }) => label)),
                          ].map((e) =>
                            filteredArr.find(({ label }) => label == e)
                          );
                          setGamesContent((prevState) => ({
                            ...prevState,
                            items: uniq,
                          }));
                        } else {
                          const filteredArrNot = GamesData.filter(
                            (ele) => ele.state !== item.slug
                          );
                          setGamesContent((prevState) => {
                            const merged = [
                              ...filteredArrNot,
                              ...prevState.items,
                            ];
                            const uniq = [
                              ...new Set(merged.map(({ label }) => label)),
                            ].map((e) =>
                              merged.find(({ label }) => label == e)
                            );
                            return {
                              ...prevState,
                              items: uniq,
                            };
                          });
                        }
                      }}
                    />
                  </div>
                );
              })}
            </FilterBoxState>
            <FilterBoxGenre>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  cursor: 'pointer',
                  marginBottom: '20px',
                }}
              >
                <h2 style={{ color: '#afafaf', fontSize: '24px' }}>
                  Genre Filters
                </h2>
                <ChevronDown color="white"></ChevronDown>
              </div>
              {genreContent?.items.map((item, index) => {
                return (
                  <div key={index} style={{ marginBottom: '10px' }}>
                    <Checkbox
                      size="md"
                      styles={{
                        root: { color: '#76b900' },
                        inner: { color: '#76b900' },
                        label: { color: '#afafaf' },
                        icon: { color: 'white' },
                      }}
                      label={item.label}
                      checked={item.checked}
                      onChange={(event) => {
                        if (event.currentTarget.checked) {
                          const filteredArr = gamesContent.items.filter(
                            (ele) => ele.genre === item.slug
                          );
                          const uniq = [
                            ...new Set(filteredArr.map(({ label }) => label)),
                          ].map((e) =>
                            filteredArr.find(({ label }) => label == e)
                          );
                          setGamesContent((prevState) => ({
                            ...prevState,
                            items: uniq,
                          }));
                        } else {
                          const filteredArrNot = GamesData.filter(
                            (ele) => ele.genre !== item.slug
                          );
                          setGamesContent((prevState) => {
                            const merged = [
                              ...filteredArrNot,
                              ...prevState.items,
                            ];
                            const uniq = [
                              ...new Set(merged.map(({ label }) => label)),
                            ].map((e) =>
                              merged.find(({ label }) => label == e)
                            );
                            return {
                              ...prevState,
                              items: uniq,
                            };
                          });
                        }
                      }}
                    />
                  </div>
                );
              })}
            </FilterBoxGenre>
          </SListFilterBox>
        </SListFilterArea>
        <SContentArea>
          <div
            style={{
              display: 'flex',
              justifyContent: 'end',
              marginBottom: '1rem',
            }}
          >
            <Select
              sx={{ color: 'black' }}
              value={sortData?.selected}
              data={sortData?.items}
              onChange={(value) => {
                setSortData((prevState) => ({
                  ...prevState,
                  selected: value,
                }));
                sortGames(value);
              }}
            />
          </div>
          {prepareData(gamesContent?.items) &&
          prepareData(gamesContent?.items).length > 0 ? (
            prepareData(gamesContent?.items).map((item, index) => {
              return (
                <div
                  key={index}
                  style={{
                    background: '#1e1e1e',
                    padding: '30px',
                    marginBottom: '10px',
                  }}
                >
                  <Hexagon>{item.letterLabel}</Hexagon>
                  <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                    {item?.games.map((item, index) => {
                      return <GamesA key={index}>{item.label}</GamesA>;
                    })}
                  </div>
                </div>
              );
            })
          ) : (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
              No Games Found
            </div>
          )}
        </SContentArea>
      </SListContainer>
    </SList>
  );
};

export default List;
