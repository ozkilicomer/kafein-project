export const StateData = [
  {
    id: 1,
    label: 'Avalible',
    slug: 'AVALIBLE',
  },
  {
    id: 2,
    label: 'Patching',
    slug: 'PATCHING',
  },
  {
    id: 3,
    label: 'Maintenance',
    slug: 'MAINTENANCE',
  },
];
