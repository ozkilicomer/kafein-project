import React from 'react';
import { SBanner, SBannerContainer, Sp } from './styles';
import { Autocomplete } from '@mantine/core';
import { Search } from 'tabler-icons-react';
import { useSelector } from 'react-redux';

const Banner = () => {
  const gamesCatalog = useSelector((state) => state.games.games);
  const gamesList = gamesCatalog.map((item) => item.label);
  return (
    <SBanner>
      <SBannerContainer>
        <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>
        <Sp>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere
          urna urna, at ullamcorper lacus mollis ac. Nullam nec mi pulvinar
          justo auctor iaculis vitae a nisi
        </Sp>
        <Autocomplete
          sx={{ width: '100%' }}
          placeholder="Search Games"
          icon={<Search />}
          data={gamesList}
          transition="pop-top-left"
          transitionDuration={80}
          transitionTimingFunction="ease"
        />
      </SBannerContainer>
    </SBanner>
  );
};

export default Banner;
