import styled from 'styled-components';

export const SBanner = styled.div`
  background: url(https://gameplus.com.tr/static/media/foot-banner.46c1fc4c.png);
  height: 400px;
  object-fit: cover;
  background-repeat: no-repeat;
  text-align: center;
  background-size: 100% 100%;
  background-position: 50%;
`;

export const SBannerContainer = styled.div`
  max-width: 793px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
  padding: 0 20px;
  color: #fff;
`;

export const Sp = styled.div`
  margin-bottom: 20px;
`;
