import React, { Suspense } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import TheLayout from './containers/TheLayout';
import routes from './routes';
import { GlobalStyles } from './styles/globalStyles';

const App = () => {
  const currentLang = useSelector((state) => state.lang.currentLang);
  localStorage.setItem('language', currentLang ?? 'tr');
  const loading = () => <div>loading...</div>;
  return (
    <>
      <GlobalStyles />
      <Helmet>
        <meta charset="utf-8" />
        <title>Kafein Project</title>
        <meta name="description" content="Kafein Project" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;700&display=swap"
          rel="stylesheet"
        ></link>
      </Helmet>
      <>
        <Suspense fallback={loading}>
          <TheLayout>
            <Switch>
              {routes.map((route, idx) => {
                return route.component ? (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={(props) => <route.component {...props} />}
                  />
                ) : null;
              })}
            </Switch>
          </TheLayout>
        </Suspense>
      </>
    </>
  );
};

export default React.memo(App);
