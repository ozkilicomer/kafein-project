import { createSlice } from '@reduxjs/toolkit';
import { GamesData } from '../../components/List/GamesData';

const gamesSlice = createSlice({
  name: 'games',
  initialState: {
    games: [...GamesData],
  },
  reducers: {},
});

export default gamesSlice;
