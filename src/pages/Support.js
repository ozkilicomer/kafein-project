import React from 'react';

const Support = () => {
  return (
    <div
      style={{
        height: 'calc(100vh -  217px)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: '#000',
        color: 'white',
      }}
    >
      Support
    </div>
  );
};

export default Support;
