import React from 'react';

const Download = () => {
  return (
    <div
      style={{
        height: 'calc(100vh -  217px)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: '#000',
        color: 'white',
      }}
    >
      Download
    </div>
  );
};

export default Download;
