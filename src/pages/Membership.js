import React from 'react';

const Membership = () => {
  return (
    <div
      style={{
        height: 'calc(100vh -  217px)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: '#000',
        color: 'white',
      }}
    >
      Membership
    </div>
  );
};

export default Membership;
