import React from 'react';

const Blog = () => {
  return (
    <div
      style={{
        height: 'calc(100vh -  217px)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: '#000',
        color: 'white',
      }}
    >
      Blog
    </div>
  );
};

export default Blog;
