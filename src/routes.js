import Home from './pages/Home';
import Membership from './pages/Membership';
import Download from './pages/Download';
import Blog from './pages/Blog';
import Support from './pages/Support';

const routes = [
  { name: 'games', path: '/', component: Home, exact: true },
  {
    name: 'membership',
    path: '/membership',
    component: Membership,
    exact: true,
  },
  { name: 'download', path: '/download', component: Download, exact: true },
  { name: 'blog', path: '/blog', component: Blog, exact: true },
  { name: 'support', path: '/support', component: Support, exact: true },
];

export default routes;
