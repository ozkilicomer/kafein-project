import React from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import { LayoutWrapper } from './styles';

const TheLayout = ({ children }) => {
  return (
    <LayoutWrapper>
      <Header></Header>
      {children}
      <Footer></Footer>
    </LayoutWrapper>
  );
};

export default TheLayout;
