import { createGlobalStyle } from 'styled-components';
import { normalize } from './normalize';

export const GlobalStyles = createGlobalStyle`
    *,
    *::after,
    *::before {
        box-sizing: border-box;
        margin: 0;
    }
    ${normalize}
    body {
        font-family: 'Roboto', sans-serif;
        letter-spacing: .6px;
    }
`;
