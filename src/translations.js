export const resources = {
  tr: {
    translation: {
      login: 'Giriş Yap',
      logout: 'Çıkış Yap',
    },
  },
  en: {
    translation: {
      login: 'Login',
      logout: 'Logout',
    },
  },
};
